from tkinter import *
from tkinter.messagebox import *

# some imp variables
font = ('Verdana', 18, 'bold')


# some imp functions
def all_clear():
    textField.delete(0, END)


def clear():
    ex = textField.get()
    ex = ex[0:len(ex) - 1]
    textField.delete(0, END)
    textField.insert(0, ex)


def click_btn_function(event):
    print("btn clicked")
    b = event.widget
    text = b['text']
    print(text)
    if text == '=':
        try:
            ex = textField.get()
            answer = eval(ex)
            textField.delete(0, END)
            textField.insert(0, answer)
        except Exception as e:
            print("Error", e)
            showerror("Error", e)
        return;
    textField.insert(END, text)


# creating a window
window = Tk()
window.title('Calculator')
window.geometry('320x360')
# picture label
#pic = PhotoImage(file='img/cal1.png')
#headingLabel = Label(window, image=pic)
#headingLabel.pack(side=TOP, pady=5)

# HEADING LABEL
heading = Label(window, text='Calculator', font=font)
heading.pack(side=TOP)

# textfield
textField = Entry(window, font=font, justify=CENTER)
textField.pack(side=TOP, pady=10)

# buttons

buttonFrame = Frame(window)
buttonFrame.pack(side=TOP)

# adding button

# b1 = Button(buttonFrame, text="1", font=font)
# b1.grid(row=0, column=0)
temp = 1
for i in range(0, 3):
    for j in range(0, 3):
        button = Button(buttonFrame, text=str(temp), font=font, width=4, relief='groove', activebackground='white')
        button.grid(row=i, column=j)
        temp = temp + 1
        button.bind('<Button-1>', click_btn_function)

zerobtn = Button(buttonFrame, text="0", font=font, width=4, relief='groove', activebackground='white')
zerobtn.grid(row=3, column=1)
dotbtn = Button(buttonFrame, text=".", font=font, width=4, relief='groove', activebackground='white')
dotbtn.grid(row=3, column=0)
eqbtn = Button(buttonFrame, text="=", font=font, width=4, relief='groove', activebackground='white')
eqbtn.grid(row=3, column=2)
plusbtn = Button(buttonFrame, text="+", font=font, width=4, relief='groove', activebackground='white')
plusbtn.grid(row=0, column=3)
minusbtn = Button(buttonFrame, text="-", font=font, width=4, relief='groove', activebackground='white')
minusbtn.grid(row=1, column=3)
multbtn = Button(buttonFrame, text="*", font=font, width=4, relief='groove', activebackground='white')
multbtn.grid(row=2, column=3)
divbtn = Button(buttonFrame, text="/", font=font, width=4, relief='groove', activebackground='white')
divbtn.grid(row=3, column=3)
clearbtn = Button(buttonFrame, text="<-", font=font, width=8, relief='groove', activebackground='white', command=clear)
clearbtn.grid(row=4, column=0, columnspan=2)
Allclearbtn = Button(buttonFrame, text="AC", font=font, width=8, relief='groove', activebackground='white',
                     command=all_clear)
Allclearbtn.grid(row=4, column=2, columnspan=2)

# binding all buttons to print

zerobtn.bind('<Button-1>', click_btn_function)
dotbtn.bind('<Button-1>', click_btn_function)
eqbtn.bind('<Button-1>', click_btn_function)
plusbtn.bind('<Button-1>', click_btn_function)
minusbtn.bind('<Button-1>', click_btn_function)
multbtn.bind('<Button-1>', click_btn_function)
divbtn.bind('<Button-1>', click_btn_function)

window.mainloop()
